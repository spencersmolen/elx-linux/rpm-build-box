# Building Docker images in GitLab CI

Demonstration of building Docker images in GitLab CI using Docker-in-Docker and Podman.

Want to learn more Docker packaging techniques for production, with a focus on Python? Visit [Production-ready Docker packaging for Python developers](https://pythonspeed.com/docker/).
